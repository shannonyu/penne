#!/usr/bin/env python

from distutils.core import setup

setup(name='penne',
      version='0.1',
      description='Python easy neural network extruder',
      author='David Chiang',
      author_email='dchiang@nd.edu',
      url='https://bitbucket.org/ndnlp/penne',
      packages=['penne'],
      )
