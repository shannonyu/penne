import importlib

_symbols = []

def select_backend(name):
    global _symbols
    g = globals()

    # Unload previously-loaded backend
    for x in _symbols:
        del g[x]

    # Load new backend
    m = importlib.import_module("." + name, __package__)
    v = vars(m)
    if hasattr(m, '__all__'):
        _symbols = m.__all__
    else:
        _symbols = [x for x in v if not x.startswith('_')]
    for x in _symbols:
        g[x] = v[x]

select_backend("backend_numpy")

def use_gpu(name='cuda0'):
    select_backend("backend_pygpu")
    init_gpu(name)

