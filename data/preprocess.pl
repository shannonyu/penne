#!/usr/bin/env perl

while (<>) {
    next if (/^\s*$/);
    next if (/^\s*(Inferno|Purgatorio|Paradiso)( \xe2\x80\xa2|:) Canto /);

    s/^/ /; s/$/ /;

    # punctuation
    s/([,.!?:;\(\)"]|-+|\xc2\xab|\xc2\xbb|\xe2\x80\x9c|\xe2\x80\x9d)/ $1 /g;
    # lowercase
    tr/A-Z/a-z/;
    # single quotes are too much trouble

    print(join(" ", split) . "\n");
}
