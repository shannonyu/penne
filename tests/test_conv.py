import unittest
import numpy
import penne
import penne.conv

class ConvTestCase(unittest.TestCase):
    def test_convolve(self):
        a = penne.parameter(numpy.random.uniform(-1., 1., (16, 16)))
        b = penne.parameter(numpy.random.uniform(-1., 1., (3, 3)))
        c = penne.conv.convolve(a, b, mode='full')
        o = penne.sum(c * penne.constant(numpy.random.uniform(-1., 1., (18, 18))))
        
        values = penne.compute_values(o)
        auto = penne.compute_gradients(o, values)
        check = penne.check_gradients(o, delta=0.1)
        error = 0.
        n = 0
        for v in a, b:
            diff = auto[v] - check[v]
            error += numpy.sum(diff * diff)
            n += diff.size
        self.assertTrue(error/n < 0.1)

    def test_max_pool(self):
        na = numpy.random.uniform(-1., 1., (16, 16, 16))
        a = penne.parameter(na)
        b = penne.conv.max_pool(a, (2, 2, 2))
        o = penne.sum(b * penne.constant(numpy.random.uniform(-1., 1., (8, 8, 8))))
        
        values = penne.compute_values(o)

        nb = values[b]

        self.assertEqual(nb.shape, (8,8,8))
        for i in xrange(8):
            for j in xrange(8):
                for k in xrange(8):
                    self.assertTrue(nb[i,j,k] == numpy.amax(na[2*i:2*(i+1),2*j:2*(j+1),2*k:2*(k+1)]))

        auto = penne.compute_gradients(o, values)
        check = penne.check_gradients(o, delta=0.1)
        error = 0.
        n = 0
        for v in [a]:
            diff = auto[v] - check[v]
            error += numpy.sum(diff * diff)
            n += diff.size
        self.assertTrue(error/n < 0.1)

cases = [
    ConvTestCase,
]

alltests = unittest.TestSuite([unittest.TestLoader().loadTestsFromTestCase(case) for case in cases])
unittest.TextTestRunner(verbosity=2).run(alltests)
